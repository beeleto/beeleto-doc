# Product requirements

## 1.0 Reservations
From this view, user performs all management of bookings and available time slots.
### 1.1 Calendar
#### 1.1.1 
Calendar allows User to see information about slots and reservations, such as their quantity and short information.  
#### 1.1.2
Top bar includes buttons Date, View, Filter, Available, Reserved, Blocked, All.
#### 1.1.3
Calendar must contain information about number of booked and available slots shown in ellipses at the top right corner of the date. Number of available shown in blue ellipse, number of reserved in orange. When slots filtered to All, the priority number is shown. When filtered by Reserved, number of reserved games shown in orange ellipses. When filtered to Available, number of available slots shown in blue ellipses. Current date highlighted in blue. 
#### 1.1.4
Detailed list of slots must contain profile picture of the listing, title of the listing, date and time, and client's name and number of participants in case of reservation. Upon clicking on a date, the list of slots show up under the calendar. Default day chosen is Today. Upon clicking on a booked slot, a pop-up window appears with the detailed information about the booking, such as Booking Details, Payment Summary, History, Communication, Notes. 
#### 1.1.5
User should be able to access the slot settings and information from the slots part of the calendar. 
#### 1.1.6
If it is an available slot, Options button offers Create Booking, Block Time Slot, Edit Slot. If slot is booked, the following options appear: Edit Booking, Cancel Booking, Contact Customer. 
#### 1.1.7
Create Booking option allows User to create a new booking for the chosen time slot. It redirects User to Add New Booking module.
#### 1.1.8
Block Time Slot option allows user to block the chosen time slot so it becomes unavailable for customers and all spaces in the slot turn to 0. The generated module allows User to block the slot on the selected date only or on all recurring slots. 
#### 1.1.9
Edit Slot option allows User to edit the chosen time slot settings. It redirects User to that listing' Settings.
#### 1.1.10
Edit Booking allows User to edit the booking. It redirects User to detailed window of the booking, where User can change experience, date and/or time, number of participants, send a message, add notes, add discount/coupon/gift card, make refund, charge amount, edit balance. 
#### 1.1.11
Cancel Booking allows User to cancel the booking so that the slot becomes available for customers. It activates pop-up window, where User is asked whether they want to cancel booking. If partly or fully paid, the system offers to refund the money either by card (if payment was made using a card) or cash (if no card was used, only cash options offered). If no payment made, no such option appears, but User can choose decrease the balance to 0. Notify Customer option either activates or disactives email notification about the cancellation. Appropriate message should notify User about the notification being sent/not sent to the customer. 
[[[[[[example]]]]]] 
#### 1.1.12
Contact Customer option allows User to send an email to the customer assigned of the booking (if email is present in the customer's information). A type-in window allows User to type in any message with no limit of symbols. 
#### 1.1.13

### 1.2 Booking Details
#### 1.2.1
Booking details must contain Booking ID.
#### 1.2.2
Booking details must contain main Listing image.
#### 1.2.3
Booking details must contain Name, email, and phone number of the customer associated with the reservation.
#### 1.2.4
Booking details must contain number of participants. For how many people is this reservation.
#### 1.2.5
Booking details must contain time and date of the service for which reservation was made.
#### 1.2.6
Booking details must contain booking status: Booked/Checked-In/No-Show, and Edit actionable button in the top right corner.  
#### 1.2.7
Booking details must contain Notes, made by the Creator at the booking process. 
#### 1.2.8
Editing customer's information on reservation while preserving customer profile and all booking history associated with them, changes to the customer profile must be done in [Customers](#30-customers) section. It will open up Customer Details pop up from Customers tab. There User can edit the customer's name, email, phone number, email.  
#### 1.2.9
Updating number of participants will open module. If Demographics is used, module will display each one with field to adjust the value. All values should follow Listing restrictions for maximum number of participants. Booking payment summary must reflect changes. If booking was paid in full and new balance is due after particiant number or demographics is adjusted, user can collect remaining balance from Payment Summary options. If balance is exceeding the total booking value, overpaid balance is reflected in payment summary and can be refunded to the customer.
#### 1.2.10
Updating time and date will open a module with date and time selectors. Only available times and dates are displayed.
#### 1.2.11
Switching booking to another type of listing (service) will open a module with drop down of available listings to choose from, date, time, and number of participant fields. Update is perfomred in steps, first user must select listing to which this booking is being switched, all other fields are not active (disabled). Upon selecting desired listing, remaining fields are updated with availability info, such as date and time. User can choose to create new time slot manually, specifically for this booking. If custom time slot is overlaping with any existing time slots taking into account duration of the serivce, message shall appear asking to reconfirm creation of custom time slot that will result in disabling intersecting slots. If there is a booking that conflicts with custom time slot, message notification will prevent using this time slot asking user to select other time. Number of participants gets prepopulated for new booking if there is no change in demographics. If new booking has different demographics, then user has to define appropriate number of participants according to the listing settings. Maximum number of participants can exceed default listing restriction, however appropriate message must be displayed to notify user (for example: 10 of 6 spaces taken for this time slot. 4 spaces over the max capacity.) Changes in booking balance are reflected based on updated  parameters and listing pricing options, user can choose to collect remaining balance from Payment Summary or refund overpaid balance.
#### 1.2.12
User should be able to change the status of booking by choosing among Upcoming, Checked-in, No-Show, Cancelled. Cancelled option should generate Cancel Booking module. Changes must be recorded in Timeline.   
#### 1.2.13
User should be able to access Notes of booking by clicking on actionable icon of the Notes. It opens up full text of the note and User can edit it by clicking Edit button.  
#### 1.2.14
User should be able to send an email to the customer. There User types in Subject and Main Body. Appropriate message notifies User if there is no email address present in the customer's information or if it is not correct. User should be able to access customer's information from Customers tab in order to add/edit email. 

### 1.3 Payment Summary
#### 1.3.1
Icon                                                | Text                                                         											    | CSS
:-------------------------------------------------: |:----------------------------------------------------------------------------------------------------------| :--------------------------------------
![Screenshot](img/card-100.png)          			| **1234* 01.01.2020 12:00 $10.99																		    | ```<i class="far fa-credit-card">```
![Screenshot](img/payment-return-100.png)           | Cash or **1234* 01.01.2020 12:00 $10.99 				         											| ```<i class="fas fa-undo-alt">```
![Screenshot](img/cash-100.png)         		    | Cash 01.01.2020 12:00 $100 				         										            	| ```<i class="fas fa-money-bill-alt">```

#### 1.3.2
Payment Summary window should include all details about any payment and edits to the balance ever done to the booking. 
#### 1.3.3 
Payment Summary must contain experience name, demographics and price per person, discounts applied, coupons applied, taxes description and amount, total amount of the booking, paid balance and payment type, due balance. 
#### 1.3.4
Payment Summary must allow User to Pay Now, Refund, and Adjust Balance. 
#### 1.3.5
Pay Now option allows User to collect the due balance. If credit card information is present, the system allows to charge it. Default amount to charge is due balance. User can change it to custom amount. If no credit card available, default option is Cash. User can add a card by manually adding card's number, expiry date and CVV. If the card is not recognized/not correct, the appropriate message should warn User. Notify Customer option activates/disactivates email notification. Appropriate message must notify User about the option's activation/disactivation. Changes should be tracked in Timeline. 
#### 1.3.6
Refund option allows User to return amount to the customer. It activates pop-up module where User chooses amount to refund (full or custom) and way to refund: cash, card, gift card. If no credit card information is present, no such option offered. Gift card option opens up New Gift Card from Marketing tab. Changes should be tracked in Timeline. 
#### 1.3.7 
Adjust Balance option allows User to edit the total balance of the booking. It generates pop-up module where User can manually edit the balance by decreasing or increasing the amount. Notify Customer options allows User to activate/disactivate sending an email to the customer with the balance change notification. Appropriate message should pop up to reflect the changes in this option. Changes should be tracked in Timeline. 
#### 1.3.6
User should be able to add/edit discount. In a pop-up module where User can change add discount by typing in the auto-suggest type-in box or choose discount amount/percentage manually. Changes should be tracked in Timeline. 
#### 1.3.7

### 1.4 Timeline
####1.4.1 
Timeline must always contain new booking event specifying booking ID and total price. 
####1.4.2 
Change of the customer in booking must be recorded in timeline. Keep track of old and new data.
####1.4.3
Change of the number of participants must be recorded in timeline. Keep track of old and new data.
####1.4.4
Change of the booking date and time must be recorded in timeline. Keep track of old and new data.
#### 1.4.5
Sending message to customer must be recorded in timeline. Keep track of old and new data.
####1.4.6

Timeline messages and icons.

Icon                                                | Text                                                         											    | CSS
:-------------------------------------------------: |:----------------------------------------------------------------------------------------------------------| :--------------------------------------
![Screenshot](img/booking-created-100.png)          | Booking *#123ABC* was created with total price *$100.00* [Requirement](requirements.md#14-timeline)       | Use *{border-radius: 5px}* to make icons in circle. ```<i class="fas fa-calendar-check">```
![Screenshot](img/booking-edit-100.png)             | Booking was switched from "Listing 1" to "Listing 1"          											| ```<i class="fas fa-pen-square">```
![Screenshot](img/booking-edit-100.png)             | Customer was updated from *Alex J* to *Peter N*		    												| ```<i class="fas fa-pen-square">```
![Screenshot](img/booking-edit-100.png)             | Time was changed from *1:00* to *12:00*			        	          								    | ```<i class="fas fa-pen-square">```
![Screenshot](img/booking-edit-100.png)             | Date was changed from *12/1/19* to *10/1/20*			                									| ```<i class="fas fa-pen-square">```
![Screenshot](img/booking-edit-100.png)             | Number of participants was updated from *5* to *15*		         									   	| ```<i class="fas fa-pen-square">```
![Screenshot](img/booking-edit-100.png)             | Date was changed from *12/1/19* to *10/1/20*			       									         	| ```<i class="fas fa-pen-square">```
![Screenshot](img/discount-100.png)             	| Discount code *#ABZ123* applied             			       									         	| ```<i class="fas fa-tags">```
![Screenshot](img/payment-100.png)              	| Refunded *$20* to Card *x3456*                 			       									       	| ```<i class="fas fa-money-bill-alt">```
![Screenshot](img/payment-100.png)              	| Card *x3456* charged *$20.00*                			       									            | ```<i class="fas fa-money-bill-alt">```

#### 1.4.7
Timeline history can be filtered by All, Transactions, Communication, Reservation. Transactions filter generates information about payments made and refunds applied. Communication filter shows any messages to/from customer. Reservation filter shows any edits done to the booking, including date, time, number of participants, experience, discounts, coupons. All filter includes all of the above information.
### 1.5 Search and Filter
#### 1.5.1 
User can filter time slots and bookings by following criteria: Reserved/Available/All/Blocked 
#### 1.5.2
Search bar allows to find existing booking by customer name, booking ID, customer email, and phone number.
#### 1.5.3
Search bar allows to select a service from a drop down list to filter bookings by. 
#### 1.5.4
Day filter/selector is located at the top of the page.
#### 1.5.5
Search by name of the customer bar allows to search a specific customer's bookings (auto-suggest type-in box). Once typed in or a customer chosen by a click, all bookings ever made by the customer appear in a pop up screen. 

## 2.0 Customers
### 2.1 
Customers tab allows User to search customers and manage their information. 
#### 2.1.0
Search bar allows User to search customers by name, phone #, email, and booking ID. The filter is auto-suggest type-in box. 
#### 2.1.1
User can switch view from List to Detailed. 
#### 2.1.2
User can sort customers from A to Z and from Z to A (by last name).  
#### 2.1.3
List view shows customers as rows. Each row must contain customer's first and last name, Email, phone number, total number of reservations, total sales.  
#### 2.1.4
Detailed view generates table, where each square is acquired by one customer. Each square must contain customer's first and last name, phone number, Email, total number of reservations, total sales. 
#### 2.1.5
User must be able to access customer's detailed information. The window must contain two sections, Customer Info and Booking History. 
#### 2.1.6
Customer Info must contain first name, last name, preferred name, phone number, email, address, credit card information. User should be able to edit customer's information. 
#### 2.1.7
Booking History must contain information about all reservations ever made by the customer. Each row must contain experience's profile picture, reservation #, date of experience, time, number of participants, total amount, due balance. 
#### 2.1.8 
From Booking History User should be able to access detailed information about each reservation. View More button. The module opens up. It must contain first and last name, reservation number, date, time, status of the reservation, total amount, due balance, transactions, discounts, coupons, gift cards, notes, communication. 
#### 2.1.9 
User should be able to Edit and Delete reservation. Edit button redirects to the booking from Reservations tab. Send Custom Email redirects User to communication part of the Customer Info page. 
#### 2.1.6


## 3.0 Listings
From this view User performs all management operations of listings. Main page contains Listings and Archived Listings tabs.   
### 3.1 Active Listings
#### 3.1.1 
Only active listings shown on this page. 
#### 3.1.2
Active Listings must contain Photo, Title, Location, Short Description, Schedule Information. 
#### 3.1.3
User should be able to Manage, Publish, Archive, Delete each listing.  
#### 3.1.4
Manage button redirects to the listing editing page. It contains the following buttons: General, Photo, Schedule, Message, Media, Button Code. 
#### 3.1.5
General part allows User to add general information about the listing. *General Information* includes fields for Title, Short Description, Long Description. *Location* allows to set up location address by manual typying and chosing on integrated Google Maps. *Participants* allows to set up minimum and maximum number of participants per listing, total number of spaces for listing. *Pricing* allows to set up price of the experience (per person/per booking/custom (allows to set up pricing changes according to the team size)). *Taxes/Fees* allows to set up/edit/add new tax group for the listing. 
#### 3.1.5
Photo part allows User to add profile picture of the listing, as well as Change and Delete it.  
#### 3.1.6
Schedule part allows User to add schedule of the listing. User can unlimited amount of schedules, until they do not intersect with each other. When a conflict occurs, appropriate watning should notify User. User sets up duration of the experience; chooses between recurring schedule or between dates; Then User chooses time slots for the newly created schedule, by checkmarking desired days of the week and then adding times to each day. User can choose dates From and Until, no N/A. User should be able to set up Block Periods, when no bookings will be accepted and no slots will be available. If Block Period intersects with a schedule, the system should warn User about the conflict. 
#### 3.1.7
Message part allows User to set up settings for the email confirmation/notification. User should be able to set up Title, Content, and Terms&Conditions for email. They can also choose different content for confirmation and edits/payments/charges notifications. 
#### 3.1.8
Media allows User to set up Facebook URL Code, Twitter URL Code, Instagram URL Code, as well as Email Sharing settings which include Email Subject and Email Content. 
#### 3.1.9
Button Code allows User to create button code and copy/paste it to website. 
#### 3.1.10
User should be able to Publish the listing. It activates the listing and its settings and schedule(s).
#### 3.1.11
User should be able to Archive listing. It moves the listing to the Archived Listings page, and disactivates the listing's schedules. If  reservations have already been made for the listing, the system warns User about it, but reservations stay active in the calendar. All soon-to-be reservations stay in the calendar and active; the profile picture of the listing for soon-to-be reservation in the calendar will be light up in light blue showing to User that the listing has been archived. Future slots disappear from the calendar and are not available for reservations. 
#### 3.1.12
User should be able to Delete listing. User will not be able to restore the listing. All soon-to-be reservations stay in the calendar and active; the profile picture of the listing in the calendar will be light up in grey showing to User that the listing has been deleted. Future slots disappear from the calendar and are not available for reservations. 

### 3.2 Archived Listings
From this view User performs management of all archived listings. 
#### 3.2.1
Only archived listings shown on this page. 
#### 3.2.2
Each listing must contain Photo, Title, Location, Short Description, Date when Archived. 
#### 3.2.3
User should be able to Activate and Delete listing.
#### 3.2.4
Activate button moves the listing to Active Listings. All settings and schedules of the listing become active. 
#### 3.2.5
Delete button deletes the listing permanently. User will not be able to restore the listing. 

## 4.0 Marketing
### 4.1 Gift Cards
From this page User can manage gift cards. Tab includes Purchased and Gift Card Campaigns. 
#### 4.1.1
User should be able to search gift cards and campaigns by gift card code, customer's name. On the right filters are located: codes from A to Z, codes from Z to A; Latest Date Created, Earliest Date Created, Created between [drop down calendar to drag-select dates]; Select All. 
#### 4.1.2
User should be able to view Purchased gift cards and Gift Card Campaigns in List and Detailed views. List view offers gift cards presented as rows with gift card code in bold, short description, date purchased by [name], spaces/amount as symbols, used as symbol , available as symbol; edit option as three-dot button. Detailed view presents gift cards as squares, each one contains gift card code in bold,  date purchased by [name], last activity, spaces/amount, used, available.
Purchased tab shows purchased and redeemed gift cards and allows User to see the details and manage. 
#### 4.1.3
User should be able to Manage, Print, and Send in Email each purchased gift card. 
#### 4.1.4
Manage redirects User to editing page, where they can change Gift Card code, Email assigned (if any); personalized message that will be sent with that email; switch back and forth between redemption type (spaces or amount); qty spaces (offered as drop down list) or value amount (type-in window); Listings (drop down list of all available (active) listings, or All); Redemption Date (All dates or From-To), Time (All times or Between Times), Days of the week (presented as switch buttons besides each day and *Apply to all days* button underneath; Expiry (drop down of *Gift Card expires on* and *Gift Card has no expiry date*). When the former is chosen, the system requires to set up the expiry date in the format of MM/DD/YEAR. 
#### 4.1.5
Print redirects User to print page, where the Gift Card is shown as PDF and can be printed from. 
#### 4.1.6
Send in Email generates module where User types in customer's email,  Subject of email and main body. Subject is prepopulated as *Your gift card is here! Redeem now*. Main Body contains prepopulated message *Take your friends and precious ones to this amazing experience!*. User can start typing in subject and main body, and the prepopulated messages will disappear, allowing User to type in their own ones. 
#### 4.1.7
Gift Card Campaigns shows gift card campaigns. Each Gift Card Campaign auto-generates gift card codes available for purchasing and redemption according to the campaign's settings.
#### 4.1.8
User should be able to search Gift Card Campaigns by gift card code, customer's name. 
#### 4.1.9 
User should be able to filter campaigns by campaign name from A to Z, campaign name from Z to A; Latest Date Created, Earliest Date Created, Created between [drop down calendar to drag-select dates]; Select All. 
#### 4.1.10
User should be able to view campaigns in List and Detailed views. List view offers campaigns presented as rows with gift campaign name in bold, short description, date created by [name], Total Purchases as symbols, Total Redemptions as symbol,Total Sales as symbol with amount; edit option as three-dot button. Detailed view presents campaigns as squares, each one contains Campaign name in bold,  date created by [name], last activity, Total Puchases; Total Redemptions; Total Sales in amount. 
#### 4.1.11
User should be able to Add New, Manage, Archive, and Delete Gift Card Campaigns. 
#### 4.1.9
Add New generates module where User must add Campaign Title, set up spaces or amount; add redemption date,time, days of the week restrictions (or choose *All*), expiry (or choose *No expiry*); custom message for emails to be sent together with the gift card; PDF gift voucher (the system generates automatic gift voucher, but User can upload their own PDF).  
#### 4.1.10
Manage generates editing module where User can edit the settings/restrictions of the campaign. 
#### 4.1.11
Archive allows User to archive a campaign. Once archived, the campaign stays in Gift Card Campaigns page, but lights up in grey. All pucrhased gift cards from the campaign stay redeemable and available in Purchased tab. All redeemed gift cards stay active and in Purchased tab.  
#### 4.1.12
Delete allows User to permanently delete a campaign. All purchased gift cards stay redeemable. New codes do not generate. User will not be able to restore the campaign. 
#### 4.1.13
 

### 4.2 Discounts/Coupons
Fron this view User performs management of Discount Groups and Coupons. 
#### 4.2.1 
Discount Groups tab allows User to create and manage discount groups. Each group is a campaign, in which all codes (created manually by User) will share the Group's settings. Codes cannot repeat.
#### 4.2.2
User should be able to search groups in an auto-suggest type-in box. 
#### 4.2.3
User should be able to filter groups by A to Z, Z to A, percentage or amount range, and by coupon code.
#### 4.2.4
User should be able to Add New group and Edit existing ones.  
#### 4.2.5
User should be able to view discount groups in List and Detailed View. 
#### 4.2.6
List view shows discount groups as rows, with discount group's name in bold, # of codes (as word), # of redeemed (as word), # of remaining (as word), edit symbol, status symbol. Default view.  
#### 4.2.7
Detailed view shows table of all discount groups in squares. Each square contains discount groups's name in bold, created(symbol) by [name], last activity (symbol) on [date], # of codes (as symbol), # of redeemed (as symbol), # of remaining (as symbol), edit symbol, status symbol. 
#### 4.2.8
User should be able to create new Discount Group. 
#### 4.2.9
User can create a new discount group by clicking Add New sign. 
#### 4.2.10
A module pops up requiring to enter group name, date range Active From (drop down calendar with drag-selecting, or button Now) and Active Until (drop down calendar with drag-selecting, or Forever), demographics (auto-suggest type in box), percentage/dollar amount, Apply To (drop down list of lisings, or All); choose Redemption Type (drop down list of Amount, Percentage, Booking Total, Spaces), type in Value Amount, choose QTY (Drop down list of Unlimited and Custom; when Custom is chosen, User has to manually type a number); Value Type (choic eof Amount,Percentage,Booking Total, Spaces; for Amount and Percentage drop down list of Per Participant amd per Booking Total shows up; for Booking Total and Spaces types no such drop down list available); Switch on/off for Limit per Booking (when switched on, a drop down list of Unlimited and Custom shows up; when Custom is clicked, User has to manually type a number) and for Limit per Customer (when switched on, a drop down list of Unlimited and Custom shows up; when Custom is clicked, User has to manually type a number). At the bottom, User chooses between Cancel and Save. The former returns User to Discount Groups page. The latter saves the group, and a new module with the codes of the group opens up. If no codes have been made yet, the message on the module says *This group does not have any codes yet. Click Add New to create a code*. Add New sign is located on the top right corner. Once clicked, new module opens up. There User has to type in Code (only letters and numbers, not case sensitive); Usage Restrictions (Unlimited or type-in box, where User types in number).  At the bottom User chooses between Cancel and Save&Update. When the former is clicked, User is redirected back to Discount Groups page. When the latter is clicked, module closes and message appears "Discount code has been successfully created". The group will be updated to reflect the newly created code. 
#### 4.2.11
User should be able to Edit a Discount Group. 
#### 4.2.12
Edit button activates a drop down list of Manage and Delete. 
#### 4.2.13
Manage opens up editing module, where User can edit any of the group's settings. At the bottom Save&Update button is located. Appropriate message shows up. To cancel without saving the changes, user can close the module. Appropriate message shows up. 
#### 4.2.14
Delete activates window, which asks "Would you like to permanently delete the group? You will not be able to restore it. Deleting this group will make its codes unavailable for future purchases. All codes purchased stay redeemable, all redeemed codes stay active". If click Yes, the group permanently deletes, message says "The group has been deleted".  If No, window closes without changes saved.
#### 4.2.15
User should be able to change the status of a group. 
#### 4.2.16
Clicking on Status of a Group symbol activates module. If the initial status is Active (green), the module will ask "Would you like to make the group inactive? All codes of the group will become inactive for future purchases/redemptions. All purchased/redeemed codes stay active". User chooses between Cancel and Yes. The former closes the module without changes. Yes switches the status to Inactive (Red) and the group becomes inactive. If the initial status is Inactive (red), the module will be asking "Would you like to make the group active?" User chooses between Cancel and Yes. The former closes the module without changes, Yes switches the status to Active (Green) and the group becomes redeemable/available for future bookings.
#### 4.2.17
Coupons tab allows User to create and manage discount coupons.  
#### 4.2.18
User should be able to search coupons in an auto-suggest type-in box. 
#### 4.2.19
User should be able to filter coupons by A to Z, Z to A, percentage or amount range, and by coupon code.
#### 4.2.20
User should be able to Add New coupon and Edit existing ones.  
#### 4.2.21
User should be able to view coupons in List and Detailed View. 
#### 4.2.22
List view shows coupons as rows, with coupon's code in bold, QTY, number of used (as word), # of available (as word), edit symbol, status symbol. Default view.  
#### 4.2.23
Detailed view shows table of all coupons in squares. Each square contains coupon's code in bold, created(symbol) by [name], last activity (symbol) on [date],  QTY (as symbol), # of used (as symbol), # of available (as symbol), edit symbol, status symbol. 
#### 4.2.24
User should be able to create new Coupon. 
#### 4.2.25
User can create a new coupon by clicking Add New sign. This opens up module where User has to enter Coupon Code, date range Active From (drop down calendar with drag-selecting, or Now) and Active Until (drop down calendar with drag-select; or Forever), demographics (auto-suggest type-in box), percentage/dollar amount, Apply To (drop down list of Listings, or All); choose Redemption Type (drop down list of Amount, Percentage, Booking Total, Spaces), type in Value Amount, choose QTY (Drop down list of Unlimited and Custom; when Custom is chosen, User has to manually type a number); Value Type (for Amount and Percentage types drop down list of Per Participant, and per Booking Total shows up; for Booking Total and Spaces types no such drop down list available); Switch on/off for Limit per Booking (when switched on, a drop down list of Unlimited and Custom shows up; when Custom is clicked, User has to manually type a number) and for Limit per Customer (when switched on, a drop down list of Unlimited and Custom shows up; when Custom is clicked, User has to manually type a number). At the bottom, User chooses between Cancel and Save. The former returns User to Discount Groups page. The latter activates message *The coupon has been successfully created* and saves the coupon;module closes up and User sees Coupons page.  
#### 4.2.26
User should be able to Edit coupons. 
#### 4.2.27
Edit button activates a drop down list of Manage and Delete. Manage opens up editing module. Delete activates window, which asks *Would you like to permanently delete the coupon? You will not be able to restore it. Deleting this coupon will make it unavailable for future purchases. All experiences purchased with the code stay active*. If click Yes, the coupon permanently deletes, message says *The coupon has been deleted*.  If No, window closes without changes saved.
#### 4.2.28
User should be able to change coupon's status. 
#### 4.2.29
Clicking on Status of a Coupon symbol activates module. If the initial status is Active (green), the module will ask *Would you like to make the coupon inactive? It will become inactive for future purchases/redemptions*. User chooses between Cancel and Yes. The former closes the module without changes. Yes switches the status to Inactive (Red) and the group becomes inactive. If the initial status is Inactive (red), the module will be asking *Would you like to make the coupon active?* User chooses between Cancel and Yes. The former closes the module without changes, Yes switches the status to Active (Green) and the coupon becomes redeemable/available for future bookings.
#### 4.2.30

### 4.3 Email Campaigns
#### 4.3.1
This tab shows all email campaigns and allows User to manage them. 
#### 4.3.2
At the top search bar is located. Email campaigns can be searched in the auto-suggest type-in box. On the right filters are located: switch A to Z/Z to A; switch Latest Created/Earliest Created. Email Campaigns are shown as a list, with the name of the campaign, short description, created by [name] on [MM/DD/YEAR], last activity: name, date; customers affected; total usage number. In the right corner Options and switch Active/Inactive buttons located. 
#### 4.3.3
When clicked Add New, a window opens up. The module has two tabs: Settings and Message. In Settings, User should type in campaign name, short description (up to 100 symbols), date effective From (drop down calendar with drag-select/button Now), date effective To (drop-down calendar with drag-select/button Forever); choose customers effected (auto-suggest type-in box/button All); Recurring Emailing with drop down list of None, Every [1,2,3,4,5,custom] month (s), Every [1,2,3,4,5,custom] day(s), Every [1,2,3,custom] year, at Time (drop down time list in HH:MM format, 12 hour format). In Message User has to type in Email Subject, Main Body (up to 300 symbols), and attach Image/PDF if desired. 
#### 4.3.4
Options button activates drop down list of Manage, Send Emails, and Delete. Manage redirects User to editing module, where all functions can be changed/edited. At the bottom User chooses between Cancel and Save&Update. The former doesn't save the changes and closes the module. The latter saves the changes and closes the module. Delete activates message that asks "Would you like to permanently delete the campaign? You will not be able to restore it". User then chooses between Cancel and Delete. Cancel closes the module, the campaign is not deleted. Delete permanently deletes the campaign and the new message shows up "The campaign has been deleted". Send Emails opens up module that asks "Would you like to manually send the emails to the assigned customers?" and options Yes and No. Yes sends the emails according to the settings, closes up the module, and new message says "The emails have been successfully sent". No closes up the module without any changes. 
#### 4.3.5
If initial status of a campaign is active (green) and User turns it to inactive (red), the status switches and the message shows up "Email campaign in inactive now. No emails will be sent from now on". If initial status is inactive (red) and User turns it to active (green), the status switches and the message shows up "Email campaign is active now". 

## 5.0 Reports
From this view User can access and download statistic reports for Transactions, Reservations, Customers, Gift Cards, Discounts.
### 5.1 Transactions 
This tab allows User to access statistics about all monetary transactions, as well as download and print.   
#### 5.1.1 
User should be able to select period for transactions.
#### 5.1.2
*Select Period*  activated drop down list of Today, Last Week, Last Month, Last Year, and Custom. Custom generates two drop down calendars, From and Until. User chooses date on both. Report is generated below. Whenever User changes date(s), they must click Update in order for the report to update.  
#### 5.1.3
User should be able to select listings for the report to sort by. Upon clicking on it, drop down of all listings is activated, with the option All at the top. 
#### 5.1.4
*Transactions Statistics* is a line graph chart, where X axis is dates and Y axis is amounts. Above the chart User can see Total Payments Made number, Total Revenue amount (Gross), Total Revenue amount (Net), Total Refunded Amount, Amount Unpaid, Total Card Payments, Total Cash Payments.
#### 5.1.5
*Transacions by Channel* pie chart shows transactions made by the customer At Checkout and Manual transactions (at backoffice/through booking). 
#### 5.1.6
*Transactions by Payment Method* bar chart shows transactions mady By Card and By Cash. Hover-over pop-up messages show the amounts for each. 
#### 5.1.7
User should be able to print the report. Upon clicking on Print sign, PDF opens up in new window. User can print the document or close the window. 
#### 5.1.8
User should be able to download the report. 
#### 5.1.9
User has to click into the box and choose from the generated drop down list of XLS, PDF, and DOC. When a format is clicked, it takes place in the box, and to the right of the box Download actionable icon is located. Upon clicking it, the report automatically downloads in the chosen format. 

### 5.2 Reservations
This tab allows User to access reservations statistics, as well as donwload and print. 
#### 5.2.1 
User should be able to select period for reservation statistics. 
#### 5.2.2
*Select Period* activates drop down list with Today, Last Week, Last Month, Last Year, and Custom. Custom generates two drop down calendars, From and Until. User chooses date on both. Report is generated below. Whenever User changes date(s), they must click Update in order for the report to update. 
#### 5.2.3
User should be able to filter reservations by Date of Purchase (Default), Date of Event. When one is active, another is disabled. When changed, udating automatically.  
#### 5.2.4
User should be able to filter reservations by listings, or select All Listings. 
#### 5.2.5
*Reservations Statistics* report is a line graph chart, where X axis is dates and Y axis is number of bookings. At the top of the chart a bar with the following information is located:  Total Number of Reservations, number of Checked-in, number of No-Show, number of Cancelled. 
#### 5.2.6
*Reservations by Status* is a pie chart, which allows User to conveniently see the distribution of Booked, Checked-In, No-Show, and Cancelled reservations. The titles and numbers are hover over popups.
#### 5.2.7
*Reservations by Listing* is a  pie chart, which allows User to conveniently see the distribution of reservations per each listing. The titles of the listings and numbers are hover over popups.
#### 5.2.8
*Demographics* is a bar chart, which allows User to see how many demographics have been part of reservations of the chosen period of time. The chart contains one demographics type per bar and the X axis shows the number of it. 
#### 5.2.9
*Reservations by Channel* is a pie chart, with Customers and Back Office sections. The titles and numbers are hover over popups. Customers sections shows the total number of reservations created by the customers (via Book Now button on the website and social media). Back Office section shows the total number of reservations made by Users. 
#### 5.2.10
User should be able to download the report. 
#### 5.2.11
User has to click into the box and choose from the generated drop down list of XLS, PDF, and DOC. When a format is clicked, it takes place in the box, and to the right of the box Download actionable icon is located. Upon clicking it, the report automatically downloads in the chosen format. 
#### 5.2.11

 
### 5.3 Customers
From this view User can access customers statistics 
#### 5.3.1
User should be able to select period for the report.
#### 5.3.2
*Select Period* activates drop down list with Today, Last Week, Last Month, Last Year, and Custom. Custom generates two drop down calendars, From and Until. User chooses date on both. User has to click Update for the changes to apply.Whenever User changes date(s), they must click Update in order for the report to update. 
#### 5.2.3
User should be able to filter customers by Date of Purchase (Default) and Date of Event. When one is chosen, another is disabled.  
#### 5.2.4
User should be able to filter the report by listing. *Listing* activates drop down list of all listings and All.User also checks/unchecks the box at *Include cancelled bookings*. 
#### 5.2.5
*Customer Statistics* is a line graph chart, where X axis is dates and Y axis is number of customers. At the top of the chart a bar with the following information is located:  New Customers, Returning Customers, Waivers Signed. 
#### 5.2.6
*New and Returning Customers* is a pie chart with the titles and qty hover over popups. 
#### 5.2.7
*Customers per Listing* is a pie chart, where each section is dedicated to one listing and shows total number of customers per that listing for the chosen period of time. The titles and qty of customers of the listings are hover over popups.
#### 5.2.8
Top Customers* is a table with the information about the customers with most spendings (Total Amount). It shows 3 top customers with the most spendings for the chosen period of time. Each row must contain customer's name and Total Amount. 
Each row (customer) has See Details and Send Message actionable icons. The first one opens up the module of that customer's info from the Customers tab. The second opens up the Send Message module. 
#### 5.2.9
User should be able to download the report. *Print* activates drop down list of XLS, PDF, and DOC. When a format is clicked, it takes place in the box, and to the right of the box Download actionable icon is located. Upon clicking it, the report automatically downloads in the chosen format. 


### 5.3 Gift Cards
This tab allows User to see statistics of purchased and redeemed/unredeemed gift cards.
#### 5.3.1
User should be able to select period for the report.  
#### 5.3.2
*Select Period* activates drop down list with Today, Last Week, Last Month, Last Year, and Custom. Custom generates two drop down calendars, From and Until. User chooses date on both. User has to click Update for the changes to apply. Whenever User changes date(s), they must click Update in order for the report to update. 
#### 5.3.3
User should be able to switch on/off Include Cancelled Reservations and Include Inactive Gift Cards. 
#### 5.3.4
*Gift Cards Statistics* is a line graph chart, where X axis is dates and Y axis is number of gift cards. At the top of the chart a bar with the following information is located: Total Number of Gift Cards Purchased, Total Value of Gift Cards Purchased, Total Number of Gift Cards Redeemed, Total Value of Gift Cards Redeemed, Value Remaining for Redemptions. 
#### 5.3.5
*Gift Cards by Status* is pie chart with sections Redeemed and Unredeemed. Numbers and amounts and titles are hover over popups. 
#### 5.3.6
*Gift Card Campaigns by Sales* is a bar chart, where every gift card campaign shown as bars on Y axis and total amount of gift cards from each campaign redeemed in the selected period of time shown on X axis.
#### 5.3.7
User should be able to download the report. *Print* activates drop down list of XLS, PDF, and DOC. When a format is clicked, it takes place in the box, and to the right of the box Download actionable icon is located. Upon clicking it, the report automatically downloads in the chosen format. 

### 5.4 Discounts/Coupons
#### 5.4.1 
Discounts/Coupons tab generates discounts and coupons statistics reports.
#### 5.4.2
At the top *Select Period* allows User to choose Period the report will be shown for. There drop down list with Today, Last Week, Last Month, Last Year, and Custom are offered. Custom generates two drop down calendars, one is From and the other is Until. User chooses date on both. On the right is Update. Report is generated below. Whenever User changes date(s), they must click Update in order for the report to update. 
#### 5.4.3
To the right filter actionable icon is located. Upon clicking, it generates drop down list of items with checkboxes: Include cancelled reservations; Include inactive discount groups; Include inactive coupons. If an item is checkmarked, it has a blue checkmark besides it. 
#### 


## 6.0 Settings (tabs horizontally, active and inactive)
### 6.1 Business
General
Currency + Format
Timezone
Date/Time Format
Location
Payments
Taxes
URL, API
Button Codes, Logs

### 6.2 Team
Team
Roles
### 6.3 Emails & Messaging
General
Content & Appearance
Confirmation
Updated email 
Cancelled booking 
Reminder
Follow-up
### 6.4 Booking Site Settings
Logo, colors
Shopping Basket settings
Support (Contact Us)
## 7.0 Profile
General
Plan 








